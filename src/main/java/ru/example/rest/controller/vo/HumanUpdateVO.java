package ru.example.rest.controller.vo;


import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.example.rest.controller.dto.HumanVO;
import ru.example.rest.controller.dto.Pagination;

import java.io.Serializable;
import java.sql.Date;

@Data
public class HumanUpdateVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Pagination pagination;
    private String name;
    private String surname;
    private String patronymic;
    private String factAddress;
    private Date birthday;
    private Long idHuman;
    private String passportIssuerBy;
    private Date passportIssuerDate;
    private String passportNumber;
    private String passportSeries;

}
