package ru.example.rest.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class RsWsJakartaApplication {
	public static void main(String[] args){
		SpringApplication.run(RsWsJakartaApplication.class, args);
	}
}
