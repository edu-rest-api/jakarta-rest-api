package ru.example.rest.controller.controller;

import org.springframework.stereotype.Component;
import ru.example.rest.controller.dto.HumanVO;
import ru.example.rest.controller.vo.HumanQueryVO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("/human")
@Produces(MediaType.APPLICATION_JSON)
public interface HumanRestService {
//    @POST
//    @Path("/{id}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    void save(@Valid @RequestBody HumanVO vO);
//
//    @DELETE
//    @Path("/{id}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    void delete(@Valid @NotNull @PathVariable("id") Long id);
//
//    @Path("/{id}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    void update(@Valid @NotNull @QueryParam("id") Long id, @Valid HumanUpdateVO vO);

    @POST
    @Path("/pagination")
    @Consumes(MediaType.APPLICATION_JSON)
    List<HumanVO> pagination(HumanQueryVO HumanQueryVO);
//
//    @Path("/{id}")
//    HumanVO getById(@Valid @QueryParam("id") Long id);
}
