package ru.example.rest.controller.controller.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Component;
import ru.example.rest.controller.controller.HumanRestService;
import ru.example.rest.controller.dto.HumanVO;
import ru.example.rest.controller.mapper.HumanMapper;
import ru.example.rest.controller.vo.HumanQueryVO;
import ru.example.rest.controller.vo.HumanUpdateVO;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class HumanRestServiceImpl implements HumanRestService {
    private final HumanMapper humanMapper;

    public void save(HumanVO vO) {
        HumanVO bean = new HumanVO();
        BeanUtils.copyProperties(vO, bean);
        humanMapper.insert(bean);
    }

    public void delete(Long id) {
        int affectRows = humanMapper.deleteByPrimaryKey(id);
        if (affectRows == 0) {
            throw new OptimisticLockingFailureException("Can't delete entity:" + id);
        }
    }

    public void update(Long id, HumanUpdateVO vO) {
        HumanVO bean = requireOne(id);
        BeanUtils.copyProperties(vO, bean);
        int affectRows = humanMapper.updateByPrimaryKeySelective(vO);
        if (affectRows == 0) {
            throw new OptimisticLockingFailureException("Can't update entity:" + id);
        }
    }

    @Override
    public List<HumanVO> pagination(HumanQueryVO human) {
        return humanMapper.searchHuman(human, human.getPagination().getPageSize());
    }

    public HumanVO getById(Long id) {
        HumanVO original = requireOne(id);
        return toDto(original);
    }

    private HumanVO toDto(HumanVO original) {
        HumanVO bean = new HumanVO();
        BeanUtils.copyProperties(original, bean);
        return bean;
    }

    private HumanVO requireOne(Long id) {
        return Optional.ofNullable(humanMapper.selectByPrimaryKey(id))
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + id));
    }
}
